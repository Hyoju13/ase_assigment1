using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Components;

namespace TesingDraw
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Drawto dw = new ASE_Components.Drawto();

            string co1 = "20";
            string co2 = "40";
            int x_axis = 10;
            int y_axis = 10;
            string color = "black";
            string shape = "drawto";
            int radius = 0;
            int width = 0;
            int height = 0;
            int hyp = 0;

            string[] value = { shape, co1, co2, color };

            dw.set(value, x_axis, y_axis, radius, width, height, hyp);
            Assert.AreEqual(value[3], dw.color);
        }
    }
}
