using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Components;

namespace TestingCircle
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Circle c = new ASE_Components.Circle();

            string rad = "50";
            int x_axis = 10;
            int y_axis = 10;
            string color = "yellow";
            string shape = "circle";
            string fill = "fillon";
            int radius = 0;
            int width = 0;
            int height = 0;
            int hyp = 0;

            string[] val = { shape, rad, color, fill };

            c.set(val, x_axis, y_axis, radius, width, height, hyp);
            Assert.AreEqual(val[3], c.fill);
        }
    }
}
