using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Components;

namespace TestingRectangle
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Rectangle c = new ASE_Components.Rectangle();

            string len = "50";
            string bre = "70";
            int x_axis = 10;
            int y_axis = 10;
            string color = "gray";
            string shape = "rectangle";
            string fill = "fillon";
            int radius = 0;
            int width = 0;
            int height = 0;
            int hyp = 0;

            string[] val = { shape, len, bre, color, fill };

            c.set(val, x_axis, y_axis, radius, width, height, hyp);
            Assert.AreEqual(val[4], c.fill);
        }
    }
}
