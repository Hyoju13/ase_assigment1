using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Components;

namespace Testingtriangle
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Triangle T = new ASE_Components.Triangle();

            string val1 = "50";
            string val2 = "70";
            string val3 = "50";
            int x_axis = 10;
            int y_axis = 10;
            string color = "gray";
            string shape = "triangle";
            string fill = "fillon";
            int radius = 0;
            int width = 0;
            int height = 0;
            int hyp = 0;

            string[] val = { shape, val1, val2, val3, color, fill };

            T.set(val, x_axis, y_axis, radius, width, height, hyp);
            Assert.AreEqual(y_axis, T.y);
        }
    }
}
