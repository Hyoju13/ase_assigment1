﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Components
{
    /// <summary>
    /// Introducing rectangle class
    /// </summary>
    public class Rectangle : FinalShape
    {
        /// <summary>
        /// Declaring integer type value
        /// </summary>
        public int len, bre, x, y;
        /// <summary>
        /// Declaring string type value
        /// </summary>
        public string color, fill;
        /// <summary>
        /// Passing the value of rectangle to finalshape
        /// </summary>
        /// <param name="respo">Array with shape, dimension, color and fill</param>
        /// <param name="x">Value of x_axis</param>
        /// <param name="y">Value of y_axis</param>
        /// <param name="radius">Value of radius</param>
        /// <param name="width">Value of width</param>
        /// <param name="height">Value of height</param>
        /// <param name="hyp">Value of hyp/Base</param>

        public void set(string[] respo, int x, int y, int radius, int width, int height, int hyp)
        {

            color = respo[3].ToLower();
            fill = respo[4].ToLower();
            this.x = x;
            this.y = y;
            if (width != 0 && height != 0)
            {
                len = width;
                bre = height;
            }
            else
            {

                len = Convert.ToInt32(respo[1]);
                bre = Convert.ToInt32(respo[2]);

            }
        }
        /// <summary>
        /// passing graphical value
        /// </summary>
        /// <param name="g">graphics variable</param>
        public void draw(Graphics g)
        {
            if (color == "yellow")

            {
                Pen p = new Pen(Color.Yellow, 5);
                SolidBrush sb = new SolidBrush(Color.Yellow);
                if (fill == "fillon")
                {
                    g.FillRectangle(sb, x, y, len, bre);
                }
                else if (fill == "filloff")
                {
                    g.DrawRectangle(p, x, y, len, bre);
                }

            }
            else if (color == "black")

            {
                Pen p = new Pen(Color.Black, 5);
                SolidBrush sb = new SolidBrush(Color.Black);
                if (fill == "fillon")
                {
                    g.FillRectangle(sb, x, y, len, bre);
                }
                else if (fill == "filloff")
                {
                    g.DrawRectangle(p, x, y, len, bre);
                }

            }
            else if (color == "gray")

            {
                Pen p = new Pen(Color.Gray, 5);
                SolidBrush sb = new SolidBrush(Color.Gray);
                if (fill == "fillon")
                {
                    g.FillRectangle(sb, x, y, len, bre);
                }
                else if (fill == "filloff")
                {
                    g.DrawRectangle(p, x, y, len, bre);
                }

            }
            else
            {
                MessageBox.Show("invalid color. Please select between yellow,gray or black.", "Alert");
            }
        }

    }
}


