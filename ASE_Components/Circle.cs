﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ASE_Components
{
    /// <summary>
    /// Introducing Circle class
    /// </summary>
    public class Circle : FinalShape
    {
        /// <summary>
        /// Declaring integer type value
        /// </summary>
        public int rad, x, y;
        /// <summary>
        /// Declaring string type value
        /// </summary>
        public string color, fill;
        /// <summary>
        /// Passing the value of circle to finalshape
        /// </summary>
        /// <param name="respo">Array with shape, dimension, color and fill</param>
        /// <param name="a">Value of x_axis</param>
        /// <param name="b">Value of y_axis</param>
        /// <param name="radius">Value of radius</param>
        /// <param name="width">Value of width</param>
        /// <param name="height">Value of height</param>
        /// <param name="hyp">Value of hyp/Base</param>
        public void set(string[] respo, int a, int b, int radius, int width, int height, int hyp)
        {



            if (radius != 0)
            {
                rad = radius;

            }
            else
            {

                rad = Convert.ToInt32(respo[1]);


            }


            this.x = a;
            this.y = b;
            color = respo[2].ToLower();
            fill = respo[3].ToLower();


        }
        /// <summary>
        /// passing graphical value
        /// </summary>
        /// <param name="g">graphics variable</param>
        public void draw(Graphics g)
        {

            if (color == "yellow")

            {
                Pen p = new Pen(Color.Yellow, 5);
                SolidBrush sb = new SolidBrush(Color.Yellow);
                if (fill == "fillon")
                {

                    g.FillEllipse(sb, x, y, rad, rad);
                }
                else if (fill == "filloff")
                {

                    g.DrawEllipse(p, x, y, rad, rad);
                }

            }
            else if (color == "black")

            {
                Pen p = new Pen(Color.Black, 5);
                SolidBrush sb = new SolidBrush(Color.Black);
                if (fill == "fillon")
                {

                    g.FillEllipse(sb, x, y, rad, rad);
                }
                else if (fill == "filloff")
                {

                    g.DrawEllipse(p, x, y, rad, rad);
                }


            }
            else if (color == "gray")

            {
                Pen p = new Pen(Color.Gray, 5);
                SolidBrush sb = new SolidBrush(Color.Gray);
                if (fill == "fillon")
                {

                    g.FillEllipse(sb, x, y, rad, rad);
                }
                else if (fill == "filloff")
                {

                    g.DrawEllipse(p, x, y, rad, rad);
                }


            }
            else
            {
                MessageBox.Show("invalid color. Please select between yellow,gray or black.", "Alert");
            }
        }


    }
}
