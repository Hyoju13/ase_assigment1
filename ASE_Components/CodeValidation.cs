﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ASE_Components
{
    /// <summary>
    /// Introducing code validation class which consist of validation(And is command parser class)
    /// </summary>
    class CodeValidation
    {
        /// <summary>
        /// Declaring string variables
        /// </summary>
        string crl;
        /// <summary>
        /// Declaring integer variables
        /// </summary>
        int place1, place2, place3;
        /// <summary>
        /// This method performs validation for text input in text box
        /// </summary>
        /// <param name="text">Text command form text box </param>
        /// <returns>Returns text from text box in array</returns>
        public String[] getValidate(String text)
        {

            string xPoint, yPoint;
            string[] retu = { };
            string[] sText = text.Split(',', ' ');
            //try
            
            if (sText[0].ToUpper() == "LOOP") // converts aplhabets to uppercase and condition is checked
            {
                if (sText.Length == 3)
                {
                    int x = Convert.ToInt32(sText[2]);// converts string value to integer and stores into variable
                    xPoint = Convert.ToString(x);
                    string[] sPoint = { "loop", xPoint };// stores in array
                    retu = sPoint;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Commmand", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "END") // converts aplhabets to uppercase and condition is checked
            {
                if (sText.Length == 1)
                {
                    string[] sPoint = { "end" };// stores in array
                    retu = sPoint;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Commmand", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "ENDMETHOD") // converts aplhabets to uppercase and condition is checked
            {
                if (sText.Length == 1)
                {
                    string[] sPoint = { "endmethod" };// stores in array
                    retu = sPoint;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Commmand", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "ENDIF") // converts aplhabets to uppercase and condition is checked
            {
                if (sText.Length == 1)
                {
                    string[] sPoint = { "endif" };// stores in array
                    retu = sPoint;
                }


            }
            else if (sText[0].ToUpper() == "IF") // converts aplhabets to uppercase and condition is checked
            {
                string shap;
                string sym;
                if (sText.Length == 5)
                {
                    int x = Convert.ToInt32(sText[3]);// converts string value to integer and stores into variable
                    if (sText[2] == "=" || sText[2] == ">" || sText[2] == "<")
                    {
                        sym = sText[2];
                        shap = sText[1];
                        xPoint = Convert.ToString(x);
                        string[] sPoint = { "if", shap, sym, xPoint };// stores in array
                        retu = sPoint;
                    }
                    else
                    {
                        MessageBox.Show("Invalid symbol");
                    }
                }
                else if (sText.Length == 8)
                {
                    int x = Convert.ToInt32(sText[3]);// converts string value to integer and stores into variable
                    if (sText[2] == "=" || sText[2] == ">" || sText[2] == "<")
                    {
                        sym = sText[2];
                        shap = sText[1];
                        xPoint = Convert.ToString(x);
                        string ci = sText[4];
                        string ra = sText[5];
                        string col = sText[6];
                        string fil = sText[7];
                        if (ci.ToLower() == "circle")
                        {
                            crl = "circle";
                        }
                        string[] sPoint = { "ifone", shap, sym, xPoint, crl, ra, col, fil };// stores in array
                        retu = sPoint;
                    }
                    else
                    {
                        MessageBox.Show("Invalid symbol");
                    }
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }



            else if (sText[0].ToUpper() == "MOVETO")
            {
                if (sText.Length < 4)
                {

                    string x = sText[1];
                    string y = sText[2];
                    try
                    {
                        if (x != null && y != null)
                        {
                            place1 = int.Parse(x);
                            place2 = int.Parse(y);
                        }
                    }
                    catch (FormatException)
                    {

                    }
                    string a1 = Convert.ToString(place1);
                    string b1 = Convert.ToString(place2);
                    string[] k = { "moveto", a1, b1 };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "RECTANGLE")
            {

                if (sText.Length < 6)

                {
                    try
                    {
                        string x = sText[1];
                        string y = sText[2];
                        string c = sText[3];
                        string f = sText[4];
                        try
                        {
                            if (x != null && y != null && c != null && f != null)
                            {
                                place1 = int.Parse(x);
                                place2 = int.Parse(y);

                            }
                        }
                        catch (FormatException)
                        {

                        }
                        string s1 = Convert.ToString(place1);
                        string s2 = Convert.ToString(place2);
                        string[] k = { "rectangle", s1, s2, c, f };
                        retu = k;
                    }
                    catch (IndexOutOfRangeException)
                    {
                        string[] sPoint = { "1", "1" };
                        retu = sPoint;
                        MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters

                    }
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "TRIANGLE")
            {
                if (sText.Length == 6)
                {
                    try
                    {


                        string x = sText[1];
                        string y = sText[2];
                        string z = sText[3];
                        string c = sText[4];
                        string f = sText[5];
                        try
                        {
                            if (x != null && y != null && y != null && z != null && c != null && f != null)
                            {
                                place1 = int.Parse(x);
                                place2 = int.Parse(y);
                                place3 = int.Parse(z);
                            }
                        }
                        catch (FormatException)
                        {

                        }
                        string s1 = Convert.ToString(place1);
                        string s2 = Convert.ToString(place2);
                        string s3 = Convert.ToString(place3);
                        string[] k = { "triangle", s1, s2, s3, c, f };
                        retu = k;
                    }
                    catch (IndexOutOfRangeException)
                    {
                        string[] sPoint = { "1", "1" };
                        retu = sPoint;
                        MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                    }
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "DRAWTO")
            {
                if (sText.Length == 4)
                {

                    string x = sText[1];
                    string y = sText[2];
                    string c = sText[3];
                    try
                    {
                        if (x != null && y != null && c != null)
                        {
                            place1 = int.Parse(x);
                            place2 = int.Parse(y);

                        }
                    }
                    catch (FormatException)
                    {

                    }
                    string x1 = Convert.ToString(place1);
                    string y1 = Convert.ToString(place2);
                    string[] k = { "drawto", x1, y1, c };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "CIRCLE")
            {

                if (sText.Length == 4)
                {
                    try
                    {

                        xPoint = sText[1];
                        string c = sText[2];
                        string f = sText[3];
                        try
                        {
                            if (xPoint != null && c != null && f != null)
                            {
                                place1 = int.Parse(xPoint);

                            }
                        }
                        catch (FormatException)
                        {

                        }
                        string radd = Convert.ToString(place1);
                        string[] k = { "circle", radd, c, f };
                        retu = k;
                    }
                    catch (IndexOutOfRangeException)
                    {
                        string[] sPoint = { "1", "1" };
                        retu = sPoint;
                        MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                    }

                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }

            }
            else if (sText[0].ToUpper() == "TRANSFORM")
            {
                if (sText.Length == 3)
                {
                    if (sText[1].Equals("xpoint") && sText[2].Equals("ypoint"))
                    {
                        xPoint = sText[1];
                        yPoint = sText[2];
                    }
                    else
                    {
                        int x = Convert.ToInt32(sText[1]);
                        int y = Convert.ToInt32(sText[2]);
                        xPoint = Convert.ToString(x);
                        yPoint = Convert.ToString(y);
                    }
                    string[] sPoint = { "transform", xPoint, yPoint };
                    retu = sPoint;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "RADIUS" && sText[1] == "=")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string rad = sText[2];
                        if (rad != null)
                        {
                            place1 = Convert.ToInt32(rad);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] sPoint = { "radius", xPoint, yPoint };
                    retu = sPoint;


                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "WIDTH" && sText[1] == "=")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string len = sText[2];
                        if (len != null)
                        {
                            place1 = int.Parse(len);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;

                    }
                    yPoint = Convert.ToString(place1);
                    xPoint = sText[1];
                    string[] sPoint = { "width", xPoint, yPoint };
                    retu = sPoint;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "HEIGHT" && sText[1] == "=")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string bre = sText[2];
                        if (bre != null)
                        {
                            place1 = int.Parse(bre);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] k = { "height", xPoint, yPoint };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "HYP" && sText[1] == "=")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string hyper = sText[2];
                        if (hyper != null)
                        {
                            place1 = int.Parse(hyper);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] k = { "hyp", xPoint, yPoint };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "HYP" && sText[1] == "+")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string bas = sText[2];
                        if (bas != null)
                        {
                            place1 = int.Parse(bas);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] k = { "hyp", xPoint, yPoint };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }

            else if (sText[0].ToUpper() == "RADIUS" && sText[1] == "+")
            {
                if (sText.Length == 3)
                {

                    string y = sText[2];
                    xPoint = sText[1];
                    try
                    {
                        if (y != null)
                        {
                            place1 = int.Parse(y);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    yPoint = Convert.ToString(place1);
                    string[] k = { "radius", xPoint, yPoint };
                    retu = k;


                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }


            else if (sText[0].ToUpper() == "WIDTH" && sText[1] == "+")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string len = sText[2];
                        if (len != null)
                        {
                            place1 = int.Parse(len);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] k = { "width", xPoint, yPoint };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }
            else if (sText[0].ToUpper() == "HEIGHT" && sText[1] == "+")
            {
                if (sText.Length == 3)
                {
                    try
                    {
                        string bre = sText[2];
                        if (bre != null)
                        {
                            place1 = int.Parse(bre);
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("invalid format");
                        place1 = 0;
                    }
                    xPoint = sText[1];
                    yPoint = Convert.ToString(place1);
                    string[] k = { "height", xPoint, yPoint };
                    retu = k;
                }
                else
                {
                    string[] sPoint = { "1", "1" };
                    retu = sPoint;
                    MessageBox.Show("Invalid Parameter");// displays message box with invalid parameters
                }
            }
            else
            {

                string[] k = { "error" };

                retu = k;
            }
            //Returning the values
            return retu;



        }
    }
}

