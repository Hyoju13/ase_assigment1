﻿using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ASE_Components
{
    /// <summary>
    /// form1 consists of all the components in all form.
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Declaring Interger variable
        /// </summary>
        int radius = 0, width = 0, height = 0, hyp = 0, b = 0, tranx = 0, trany = 0, xa, ya;
        /// <summary>
        /// Declaring Graphical variables
        /// </summary>
        Graphics g;
        /// <summary>
        /// Declaring string variable
        /// </summary>
        string textOrder, input;
        /// <summary>
        /// Declaring value for x_axis and y_axis
        /// </summary>
        int x_axis = 0, y_axis = 0;

        Pen p = new Pen(Color.Black, 5);
       /// <summary>
       /// Initializing form 1
       /// </summary>
        public Form1()
        {
            InitializeComponent();
            g = panel1.CreateGraphics();
        }
        /// <summary>
        /// This event is to exit from the program
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// for about tool menu
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is fantastic program by @Aniket Hyoju", "About");
        }
        /// <summary>
        /// This event is used to save text from text box
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File (.txt)| *.txt";
            saveFileDialog.Title = "Save File...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = new StreamWriter(saveFileDialog.FileName);
                fWriter.Write(txtmulti.Text);
                fWriter.Close();
                MessageBox.Show("Command saved", "Message");
            }
         
        }
        /// <summary>
        /// This event is used to load text in text box
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>

        private void loadToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog loadFileDialog = new OpenFileDialog();
            loadFileDialog.Filter = "Text File (.txt)|*.txt";
            loadFileDialog.Title = "Open File...";

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = new System.IO.StreamReader(loadFileDialog.FileName);
                txtmulti.Text = streamReader.ReadToEnd();
                streamReader.Close();
                MessageBox.Show("Command loaded", "Message");
            }
           
        }
        /// <summary>
        /// method for button clicking
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtsingle.Text == "run")
            {
                if (!string.IsNullOrEmpty(txtmulti.Text))// checking the text box is null or not.
                {
                    string myCmd = txtmulti.Text;
                    if (!String.IsNullOrEmpty(myCmd))
                    {

                        string[] Cmd = myCmd.Split(',', ' ', '=', '+');

                        if (Cmd[0].ToLower() == "method")
                        {
                            methodCommand();
                        }
                        else
                        {
                            textOrder = txtmulti.Text.Trim();

                            string[] arrayOrder = Regex.Split(textOrder, "\n");

                            for (int i = 0; i < arrayOrder.Length; i++)
                            {
                                CodeValidation v = new CodeValidation();
                                //commands validation in class codevalidation.
                                String[] result = v.getValidate(arrayOrder[i]);
                                getShape(result);

                            }
                        }
                    }
                }
                else
                {
                    //error messgae if command field is empty
                    MessageBox.Show("The multiple command field is empty!", "Empty command",
                                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                }
            }
            else if (txtsingle.Text == "clear".ToLower())
            {
                //to clear the panel 
                panel1.Refresh();
                // clears the multiple txt box
                txtmulti.Clear();
            }
            else if (txtsingle.Text == "reset".ToLower())
            {
                //Reset value of listed variables to 0.
                x_axis = 0;
                y_axis = 0;
                radius = 0;
                width = 0;
                height = 0;
                hyp = 0;
            }
            else
            {
                //Error message if single line command is empty
                MessageBox.Show("Single line command missing", "Empty command",
                                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// this method is called when values are returned from the codevalidationclass.
        /// </summary>
        /// <param name="result">Array stores returned values from codevalidation class. </param>
        public void getShape(string[] result)
        {
            try
            {

                if (result[0] == "radius" && result[1] == "=")
                {
                    radiusvariable(result);

                }
                else if (result[0] == "radius" && result[1] == "+")
                {
                    radiusadd(result);

                }
                if (result[0] == "hyp" && result[1] == "=")
                {
                    hypervariable(result);

                }
                else if (result[0] == "hyp" && result[1] == "+")
                {
                    hyperadd(result);

                }
                else if (result[0] == "width" && result[1] == "=")
                {
                    widthvariable(result);


                }
                else if (result[0] == "height" && result[1] == "=")
                {
                    heightvariable(result);


                }


                else if (result[0] == "width" && result[1] == "+")
                {
                    widthadd(result);
                }
                else if (result[0] == "height" && result[1] == "+")
                {
                    heightadd(result);
                }

                else if (result[0] == "loop")
                {
                    loop(result, radius, width, height, hyp);
                }
                else if (result[0] == "if")
                {
                    ifcase(result);
                }
                else if (result[0] == "ifone")
                {
                    ifonecase(result);
                }
                else if (result[0] == "circle")
                {
                    circle(result);
                }

                else if (result[0] == "moveto")
                {
                    moveto(result);
                }
                else if (result[0] == "drawto")
                {
                    drawto(result);
                }
                else if (result[0] == "transform")
                {
                    Transform(result);
                }
                else if (result[0] == "rectangle")
                {
                    rectangle(result);
                }
                else if (result[0] == "triangle")
                {
                    triangle(result);
                }


                else if (result[0] == "error")
                {
                    MessageBox.Show("Error in command", "ALERT");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in command");
            }

        }


        /// <summary>
        /// Method is for addition of radius
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void radiusadd(string[] result)
        {
            //Addition of Radius
            radius = radius + Convert.ToInt32(result[2]);


        }
        /// <summary>
        /// Method is called for Initializing radius
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class </param>
        public void radiusvariable(string[] result)
        {
            //Radius Initialized
            radius = Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is for addition of Base/Hypotenuse
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void hyperadd(string[] result)
        {
            //Addition of Base/Hypotenuse
            hyp = hyp + Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is called for Initializing Hypotenuse/Base
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>

        public void hypervariable(string[] result)
        {
            //Hypotenuse/Base Initialized
            hyp = Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is called for Initializing height
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void heightvariable(string[] result)
        {
            //Height Initialized
            height = Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is for addition of height
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void heightadd(string[] result)
        {
            //Addition of Height
            height = height + Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is called for Initializing width
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void widthvariable(string[] result)
        {
            //Width Initialized
            width = Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// Method is for addition of width
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void widthadd(string[] result)
        {
            //Addition of Width
            width = width + Convert.ToInt32(result[2]);

        }
        /// <summary>
        /// This method is called after circle is returned from codevalidation class and setting values to draw a circle on the drawing panel
        /// </summary>
        /// <param name="result">Stores the returned values from codevalidation class</param>
        public void circle(string[] result)
        {

            ShapeFactory c1 = new ShapeFactory();
            FinalShape sh = c1.getShape(result[0]);
            sh.set(result, x_axis, y_axis, radius, width, height, hyp);
            sh.draw(g);
        }

        /// <summary>
        /// Method to move x_axis and y_axis
        /// </summary>
        /// <param name="result">coordinates stored in array result</param>

        public void moveto(string[] result)
        {

            int x_axis1 = Convert.ToInt32(result[1]);
            int y_axis1 = Convert.ToInt32(result[2]);
            x_axis = x_axis1;
            y_axis = y_axis1;
        }
        /// <summary>
        /// method is called after triangle is returned from codevalidation class and setting values to draw a triangle on the drawing panel
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void triangle(string[] result)
        {
            ShapeFactory t1 = new ShapeFactory();
            FinalShape sh = t1.getShape(result[0]);
            sh.set(result, x_axis, y_axis, radius, width, height, hyp);
            sh.draw(g);
        }
        /// <summary>
        /// Method is called after drawto is returned from codevalidation class and setting values to draw a line on the drawing panel
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void drawto(string[] result)
        {
            ShapeFactory t1 = new ShapeFactory();
            FinalShape sh = t1.getShape(result[0]);
            sh.set(result, x_axis, y_axis, radius, width, height, hyp);
            sh.draw(g);
        }
        /// <summary>
        /// Method is called after Transform is returned from codevalidation class and setting values to transform the 
        /// shape on the drawing panel
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        public void Transform(string[] result)
        {
            string[] line;
            line = txtmulti.Lines;
            CodeValidation val = new CodeValidation();
            var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            int length1 = textArr.Length;
            int counter = 0;
            for (int t = 0; t < length1 - 1; t++)
            {
                string[] arrayOrder = Regex.Split(line[counter], "\r\n");//splits the data at next line and stores themin array
                string[] result1 = val.getValidate(arrayOrder[0]);
                if (result1[0] == "circle")
                {
                    tranx = Convert.ToInt32(result[1]);
                    trany = Convert.ToInt32(result[2]);
                    ShapeFactory c1 = new ShapeFactory();
                    FinalShape sh = c1.getShape(result1[0]);
                    sh.set(result1, tranx, trany, radius, width, height, hyp);
                    sh.draw(g);

                }
                else if (result1[0] == "rectangle")
                {
                    tranx = Convert.ToInt32(result[1]);
                    trany = Convert.ToInt32(result[2]);
                    ShapeFactory c1 = new ShapeFactory();
                    FinalShape sh = c1.getShape(result1[0]);
                    sh.set(result1, tranx, trany, radius, width, height, hyp);
                    sh.draw(g);

                }
                else if (result1[0] == "triangle")
                {
                    tranx = Convert.ToInt32(result[1]);
                    trany = Convert.ToInt32(result[2]);
                    ShapeFactory c1 = new ShapeFactory();
                    FinalShape sh = c1.getShape(result1[0]);
                    sh.set(result1, tranx, trany, radius, width, height, hyp);
                    sh.draw(g);

                }
                counter = counter + 1;
            }

        }
        /// <summary>
        /// This method is called after rectangle is returned from codevalidation class and setting values to draw a rectangle on the drawing panel
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>

        public void rectangle(string[] result)
        {
            ShapeFactory s1 = new ShapeFactory();
            FinalShape sh = s1.getShape(result[0]);
            sh.set(result, x_axis, y_axis, radius, width, height, hyp);
            sh.draw(g);
        }

        /// <summary>
        /// This method is called while ending the if and loop statement
        /// </summary>
        /// <returns>stores the returned values from codevalidation class</returns>
        public int Ending()
        {
            try
            {
                string[] line;
                line = txtmulti.Lines;
                CodeValidation val = new CodeValidation();
                var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                int length = textArr.Length;
                int counter = 0;
                //Validating each and every line using loop from array
                for (int t = 1; t <= length; t++)
                {
                    string[] arrayOrder = Regex.Split(line[counter], "\r\n");//splits the data at next line and stores them in array
                    string[] result1 = val.getValidate(arrayOrder[0]);
                    if (result1[0] == "end")
                    {
                        return 5;

                    }

                    if (result1[0] == "endif")
                    {
                        return 7;

                    }

                    counter = counter + 1;
                }



            }
            catch (NullReferenceException e)
            {
                MessageBox.Show(e.Message);
            }

            return 0;
        }


        /// <summary>
        /// This method is called for indicating line number in loop
        /// </summary>
        /// <returns>returns the line number</returns>
        private int linenumber()
        {
            string[] line;
            line = txtmulti.Lines;
            CodeValidation val = new CodeValidation();
            var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            int length1 = textArr.Length;
            int counter = 0;
            //Validating each and every line using loop from textbox
            for (int t = 0; t <= length1; t++)
            {
                string[] arrayOrder = Regex.Split(line[counter], "\r\n");//splits the data at next line and stores themin array

                string[] result1 = val.getValidate(arrayOrder[0]);
                if (result1[0] == "loop")
                {

                    return counter;

                }



                counter = counter + 1;

            }
            return 0;
        }
        /// <summary>
        /// This method performs the loop command
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        /// <param name="radius">stores the value from radius</param>
        /// <param name="width">stores the value from width</param>
        /// <param name="height">stores the value from height</param>
        /// <param name="hyp">stores the value from hyp</param>

        private void loop(string[] result, int radius, int width, int height, int hyp)
        {

            try
            {
                b = Ending();

                if (b == 5)
                {
                    string[] line;
                    //converting the value into integer
                    int a = Convert.ToInt32(result[1]);
                    line = txtmulti.Lines;
                    CodeValidation val = new CodeValidation();
                    var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    int length = textArr.Length;
                    //Repeting the required process for number of loops.
                    for (int s = 1; s <= a; s++)
                    {
                        int temp = linenumber();

                        int counter = 0;
                        for (int t = 1; t < length - 1; t++)
                        {
                            string[] arrayOrder = Regex.Split(line[temp], "\r\n");//storing data in array by spliting them at next line

                            String[] results = val.getValidate(arrayOrder[0]);


                            if (results[0] == "rectangle")
                            {
                                rectangle(results);
                            }
                            else if (results[0] == "triangle")
                            {
                                triangle(results);
                            }
                            else if (results[0] == "circle")
                            {
                                circle(results);
                            }

                            else if (results[0] == "transform")
                            {
                                Transform(results);
                            }
                            else if (results[0] == "drawto")
                            {
                                drawto(results);
                            }
                            else if (results[0] == "moveto")
                            {
                                moveto(results);
                            }
                            else if (results[0] == "radius" && results[1] == "=")
                            {
                                string[] r = { "radius", results[1], results[2] };
                                radiusvariable(r);

                            }
                            else if (results[0] == "width" && results[1] == "=")
                            {

                                string[] r = { "width", results[1], results[2] };
                                widthvariable(r);
                            }
                            else if (results[0] == "height" && results[1] == "=")
                            {

                                string[] r = { "height", results[1], results[2] };
                                heightvariable(r);
                            }
                            else if (results[0] == "radius" && results[1] == "+")
                            {
                                string[] r = { "radius", results[1], results[2] };
                                radiusadd(r);
                            }
                            else if (results[0] == "width" && results[1] == "+")
                            {

                                string[] r = { "width", results[1], results[2] };
                                widthadd(r);
                            }
                            else if (results[0] == "height" && results[1] == "+")
                            {

                                string[] r = { "height", results[1], results[2] };
                                heightadd(r);
                            }
                            else if (results[0] == "hyp" && results[1] == "=")
                            {

                                string[] r = { "hyp", results[1], results[2] };
                                hypervariable(r);
                            }
                            else if (results[0] == "hyp" && results[1] == "+")
                            {
                                string[] r = { "hyp", results[1], results[2] };
                                hyperadd(r);
                            }

                            counter = counter + 1;
                            temp = temp + 1;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Missing end command", "ALERT");

                }



            }
            catch (NullReferenceException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Method performs the if commands for single line
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        private void ifonecase(string[] result)
        {
            string[] line;
            line = txtmulti.Lines;
            CodeValidation val = new CodeValidation();
            var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            int length = textArr.Length;

            int counter = 0;
            for (int s = 1; s <= length; s++)
            {
                string[] arrayOrder = Regex.Split(line[counter], "\r\n");//storing data in array by spliting them at next line
                String[] results = val.getValidate(arrayOrder[0]);


                if (results[1] == "radius")
                {
                    symbolcheckforone(results);
                }

                counter = counter + 1;


            }
        }
        /// <summary>
        /// This method checks the values and symbols for one line command
        /// </summary>
        /// <param name="results">stores the returned values from codevalidation class</param>
        private void symbolcheckforone(string[] results)
        {


            if (results[2] == "=")
            {
                if (results[1] == "radius")
                {
                    if (Convert.ToInt32(results[3]) == radius)
                    {
                        MessageBox.Show("Radius equal");
                        ShapeFactory c1 = new ShapeFactory();
                        FinalShape sh = c1.getShape("circle");
                        string[] one = { "circle", "r", results[6], results[7] };
                        sh.set(one, x_axis, y_axis, radius, width, height, hyp);
                        sh.draw(g);
                    }
                    else
                    {
                        MessageBox.Show("Radius not equal");
                    }
                }
            }
            else if (results[2] == ">")
            {

                if (results[1] == "radius")
                {
                    if (radius > Convert.ToInt32(results[3]))
                    {

                        ShapeFactory c1 = new ShapeFactory();
                        FinalShape sh = c1.getShape("circle");
                        string[] one = { "circle", "r", results[6], results[7] };
                        sh.set(one, x_axis, y_axis, radius, width, height, hyp);
                        sh.draw(g);
                    }
                    else
                    {
                        MessageBox.Show("radius is smaller");
                    }
                }
            }
            else if (results[2] == "<")
            {
                if (results[1] == "radius")
                {
                    if (radius < Convert.ToInt32(results[3]))
                    {

                        ShapeFactory c1 = new ShapeFactory();
                        FinalShape sh = c1.getShape("circle");
                        string[] one = { "circle", "r", results[6], results[7] };
                        sh.set(one, x_axis, y_axis, radius, width, height, hyp);
                        sh.draw(g);
                    }
                    else
                    {
                        MessageBox.Show("radius is greater");
                    }
                }
            }

        }
        /// <summary>
        /// This method performs the if command for block with endif
        /// </summary>
        /// <param name="result">stores the returned values from codevalidation class</param>
        private void ifcase(string[] result)
        {

            int b = Ending();
            if (b == 7)
            {
                string[] line;
                line = txtmulti.Lines;
                CodeValidation val = new CodeValidation();
                var textArr = textOrder.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                int length = textArr.Length;


                int counter = 0;
                for (int s = 1; s < length - 1; s++)
                {
                    string[] arrayOrder = Regex.Split(line[counter], "\r\n");//splits the data at next line and stores themin array
                    String[] results = val.getValidate(arrayOrder[0]);


                    if (results[1] == "radius")
                    {
                        symbolcheck(results, line, counter);
                    }

                    counter = counter + 1;


                }
            }
            else
            {
                //Error message when endif command is missing
                MessageBox.Show("endif missing", "ALERT");
            }

        }
        /// <summary>
        /// This method checks the value of symbol and radius for if command with endif block.
        /// </summary>
        /// <param name="results">stores the returend values from codevalidation class</param>
        /// <param name="line">store the lines</param>
        /// <param name="counter">store the number of lines</param>
        private void symbolcheck(string[] results, string[] line, int counter)
        {
            CodeValidation val = new CodeValidation();

            if (results[2] == "=")
            {


                if (results[1] == "radius")
                {
                    if (Convert.ToInt32(results[3]) == radius)
                    {
                        MessageBox.Show("Radius equal");
                        string[] arrayOrders = Regex.Split(line[counter + 1], "\r\n");//splits the data at next line and stores themin array

                        String[] result = val.getValidate(arrayOrders[0]);
                        if (result[0] == "circle")
                        {

                            string[] value = { "circle", result[1], result[2], result[3] };

                            circle(value);
                        }
                    }
                    else
                    {
                        //Error message when radius is not equal in if case command
                        MessageBox.Show("Radius is not equal","Alert",MessageBoxButtons.RetryCancel,MessageBoxIcon.Error);

                    }
                }
            }
            else if (results[2] == ">")
            {

                if (results[1] == "radius")
                {
                    if (radius > Convert.ToInt32(results[3]))
                    {

                        string[] arrayOrders = Regex.Split(line[counter + 1], "\r\n");//splits the data at next line and stores themin array
                        String[] result = val.getValidate(arrayOrders[0]);


                        if (result[0] == "circle")
                        {

                            string[] value = { "circle", result[1], result[2], result[3] };
                            circle(value);
                        }
                    }
                    else
                    {
                        //Error message when radius is small
                        MessageBox.Show("radius is smaller ", "Alert", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                    }
                }

            }

            else if (results[2] == "<")
            {
                if (results[1] == "radius")
                {
                    if (radius < Convert.ToInt32(results[3]))
                    {
                        string[] arrayOrders = Regex.Split(line[counter + 1], "\r\n");//splits the data at next line and stores themin array
                        String[] r = val.getValidate(arrayOrders[0]);
                        r = val.getValidate(arrayOrders[0]);

                        if (r[0] == "circle")
                        {
                            string[] value = { "circle", r[1], r[2], r[3] };
                            circle(value);
                        }
                    }
                    else
                    {
                        //Error message when radius is greater
                        MessageBox.Show("radius is greater ", "Alert", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                    }
                }
            }
        }
        /// <summary>
        /// method is for calling methodcommand in program.
        /// </summary>
        private void methodCommand()
        {

            string command = txtmulti.Text;

            int ra = 50;
            int le = 50, be = 60;
            string[] commandline = command.Split('\n');
            string[] Cmd = commandline[0].Split(',', ' ', '(', ')');
            string endmethod = commandline[commandline.Length - 2];
            string callmethod = commandline[commandline.Length - 1];

            if (!endmethod.Contains("endmethod"))
            {
                //checking whether the command line consist endmethod.
                MessageBox.Show("The 'method' must end with 'endmethod'. Please check the command again! or plaese try calling the method" + endmethod,
                    "Invalid method", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }
            else
            {
                string[] m = callmethod.Split('(', ',', ')');
                if (m[0] != Cmd[1])
                {
                    //checking whether the method name is same or different.
                    MessageBox.Show("Name of the Method could not be recognized. Please try again!!" + Cmd[1]);
                }
                else
                {
                    int parachecker = m.Length + 1;
                    if (Cmd.Length != parachecker)
                    {
                        //Error message when the parameter is passed wrong.
                        MessageBox.Show("Fault in Argument", "Alert", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (Cmd.Length < 2)
                        {
                            MessageBox.Show("Enter valid method command. Please check the command again!",
                            "Invalid method", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                        }
                        else
                        {
                            int c = commandline.Length - 1;
                            for (int j = 1; j < c; j++)
                            {
                                input = commandline[j];
                                string[] sth = input.Split(' ', ',');

                                if (sth.Length == 5)
                                {
                                    if (sth[0].ToLower() == "rectangle")
                                    {
                                        string len = m[1];
                                        string bre = m[2];
                                        string col = m[3];
                                        string fil = m[4];

                                        string[] values = { "rectangle", len, bre, col, fil };
                                        rectangle(values);
                                      

                                    }
                                }
                                else if (sth.Length == 3)
                                {
                                    if (sth[0].ToLower() == "moveto")
                                    {
                                        xa = int.Parse(m[1]);
                                        ya = int.Parse(m[2]);
                                        string xaxis = Convert.ToString(xa);
                                        string yaxis = Convert.ToString(ya);
                                        string[] para = { "moveto", xaxis, yaxis };
                                        moveto(para);

                                    }
                                }
                                else if (sth.Length == 4)
                                {
                                    if (sth[0].ToLower() == "drawto")
                                    {
                                        string p1 = m[1];
                                        string p2 = m[2];
                                        string col = m[3];

                                        string[] para = { "drawto", p1, p2, col };
                                        drawto(para);

                                    }



                                    else if (sth[0].ToLower() == "circle")
                                    {
                                        string rad = m[1];
                                        string col = m[2];
                                        string fil = m[3];

                                        string[] values = { "circle", rad, col, fil };
                                        circle(values);

                                    }

                                }
                                else if (sth.Length == 6)
                                {
                                    if (sth[0].ToLower() == "triangle")
                                    {
                                        string side1 = m[1];
                                        string side2 = m[2];
                                        string side3 = m[3];
                                        string col = m[4];
                                        string fil = m[5];


                                        string[] values = { "triangle", side1, side2, side3, col, fil };
                                        triangle(values);


                                    }
                                }
                                else if (sth.Length == 2)
                                {
                                    if (sth[0].ToLower() == "circle")
                                    {



                                        g.DrawEllipse(p, x_axis, y_axis, ra, ra);

                                    }
                                    else if (sth[0].ToLower() == "rectangle")
                                    {



                                        g.DrawRectangle(p, x_axis, y_axis, le, be);

                                    }
                                    else if (sth[0].ToLower() == "triangle")
                                    {



                                        Point[] ptn = new Point[] { new Point(10, 100), new Point(70, 100), new Point(40, 0) };
                                        g.DrawPolygon(p, ptn);

                                    }
                                    else if (sth[0].ToLower() == "drawto")
                                    {



                                        g.DrawLine(p, x_axis, y_axis, le, be);

                                    }
                                }
                            }
                        }

                    }
                }
            }

        }
    }
}
