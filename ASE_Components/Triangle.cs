﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Components
{
    /// <summary>
    /// Introducing triangle class
    /// </summary>
    public class Triangle : FinalShape
    {
        /// <summary>
        /// Declaring integer type value
        /// </summary>
        public int val1, val2, val3, x, y;
        /// <summary>
        ///  Declaring string type variable
        /// </summary>
        public string fills, col;
        /// <summary>
        /// Passing the value of rectangle to finalshape
        /// </summary>
        /// <param name="respo">Array with shape, dimension, color and fill</param>
        /// <param name="x">Value of x_axis</param>
        /// <param name="y">Value of y_axis</param>
        /// <param name="radius">Value of radius</param>
        /// <param name="width">Value of width</param>
        /// <param name="height">Value of height</param>
        /// <param name="hyp">Value of hyp/Base</param>
        public void set(string[] respo, int x, int y, int radius, int width, int height, int hyp)
        {

            col = respo[4].ToLower();
            fills = respo[5].ToLower();
            this.x = x;
            this.y = y;
            if (width != 0 && height != 0 && hyp != 0)
            {
                val1 = width;
                val2 = height;
                val3 = hyp;
            }
            else
            {


                val1 = Convert.ToInt32(respo[1]);
                val2 = Convert.ToInt32(respo[2]);
                val3 = Convert.ToInt32(respo[3]);

            }
        }
        /// <summary>
        /// passing graphical value
        /// </summary>
        /// <param name="g">graphics variable</param>
        public void draw(Graphics g)
        {
            if (col == "gray")
            {
                Pen p = new Pen(Color.Gray, 5);
                SolidBrush sb = new SolidBrush(Color.Gray);
                PointF A = new Point(x, y);
                PointF B = new PointF(x + val3, y);
                PointF C = new PointF(B.X, B.Y + val3);
                PointF[] cho = { A, B, C };
                if (fills == "fillon")
                {
                    g.FillPolygon(sb, cho);
                }
                else if (fills == "filloff")
                {
                    g.DrawPolygon(p, cho);
                }

            }
            else if (col == "black")
            {
                Pen p = new Pen(Color.Black, 5);
                SolidBrush sb = new SolidBrush(Color.Black);
                PointF A = new Point(x, y);
                PointF B = new PointF(x + val3, y);
                PointF C = new PointF(B.X, B.Y + val3);
                PointF[] cho = { A, B, C };
                if (fills == "fillon")
                {
                    g.FillPolygon(sb, cho);
                }
                else if (fills == "filloff")
                {
                    g.DrawPolygon(p, cho);
                }

            }
            else if (col == "yellow")
            {
                Pen p = new Pen(Color.Yellow, 5);
                SolidBrush sb = new SolidBrush(Color.Yellow);
                PointF A = new Point(x, y);
                PointF B = new PointF(x + val3, y);
                PointF C = new PointF(B.X, B.Y + val3);
                PointF[] cho = { A, B, C };
                if (fills == "fillon")
                {
                    g.FillPolygon(sb, cho);
                }
                else if (fills == "filloff")
                {
                    g.DrawPolygon(p, cho);
                }

            }
            else
            {
                MessageBox.Show("invalid color. Please select between yellow,gray or black.", "Alert");
            }
        }

    }
}
