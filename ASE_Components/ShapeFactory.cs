﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Components
{
    /// <summary>
    /// Declaring shapeFactory class
    /// </summary>
    class ShapeFactory
    {
        /// <summary>
        /// Checking the type of shape insert
        /// </summary>
        /// <param name="input">Text input for shapes</param>
        /// <returns>return shape object</returns>
        public FinalShape getShape(String input)
        {
            {
                if (input == "rectangle")
                {
                    return new Rectangle();
                }
                else if (input == "circle")
                {
                    return new Circle();
                }
                else if (input == "triangle")
                {
                    return new Triangle();
                }
                else if (input == "drawto")
                {
                    return new Drawto();
                }
                return null;
            }
        }
    }
}
