﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Components
{
    /// <summary>
    /// Introducing Drawto class
    /// </summary>
    public class Drawto : FinalShape
    {
        /// <summary>
        /// Declaring integer type value
        /// </summary>
        public int a, b, x, y;
        /// <summary>
        /// Declaring String type value
        /// </summary>
        public string color;
        /// <summary>
        /// Passing the value to finalshape
        /// </summary>
        /// <param name="res">Array with shape, dimension, color and fill</param>
        /// <param name="x">Value of x_axis</param>
        /// <param name="y">Value of y_axis</param>
        /// <param name="radius">Value of radius</param>
        /// <param name="width">Value of width</param>
        /// <param name="height">Value of height</param>
        /// <param name="hyp">Value of hyp/Base</param>
        public void set(string[] res, int x, int y, int radius, int width, int height, int hyp)
        {
            if (width != 0 && height != 0)
            {
                a = width;
                b = height;
            }
            else
            {

                a = Convert.ToInt32(res[1]);
                b = Convert.ToInt32(res[2]);


            }

            color = res[3].ToLower();
            this.x = x;
            this.y = y;
        }
        /// <summary>
        /// passing graphical value
        /// </summary>
        /// <param name="g">graphics variable</param>
        public void draw(Graphics g)
        {
            if (color == "yellow")

            {
                Pen p = new Pen(Color.Yellow, 5);
                g.DrawLine(p, x, y, a, b);
            }
            else if (color == "black")

            {
                Pen p = new Pen(Color.Black, 5);
                g.DrawLine(p, x, y, a, b);
            }
            else if (color == "gray")

            {
                Pen p = new Pen(Color.Gray, 5);
                g.DrawLine(p, x, y, a, b);
            }

        }

    }

}