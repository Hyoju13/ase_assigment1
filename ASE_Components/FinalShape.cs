﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace ASE_Components
{
    /// <summary>
    /// Declaring Finalshape class
    /// </summary> 
    interface FinalShape
    {
        /// <summary>
        /// Passing the value to finalshape
        /// </summary>
        /// <param name="respo">Array with shape, dimension, color and fill</param>
        /// <param name="a">Value of x_axis</param>
        /// <param name="b">Value of y_axis</param>
        /// <param name="radius">Value of radius</param>
        /// <param name="width">Value of width</param>
        /// <param name="height">Value of height</param>
        /// <param name="hyp">Value of hyp/Base</param>
        void set(String[] respo, int a, int b, int radius, int width, int height, int hyp);
        /// <summary>
        /// passing graphical value
        /// </summary>
        /// <param name="g">graphics variable</param>
        void draw(Graphics g);
    }
}
